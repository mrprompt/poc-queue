const queue = require('bee-queue');
const faker = require('faker');

queue
  .connect()
  .then((channel) => {
    var MAX = 100;

    for (var i = 0; i <= MAX; i++) {
      const msg = {
        message: {
          CUSTOMER_NAME: faker.name.findName(),
          CPF_CNPJ: faker.random.number(),
          email: faker.internet.email(),
        },
        template: 'customer-welcome-mail'
      };

      queue.addMessage(channel, new Buffer(JSON.stringify(msg)));

      console.log("[x] Sent %s. To exit press CTRL+C", msg.message.email);
    }
  })
  .catch(function (err) {
    console.error('putz:', err.message);
  });
