const async = require('async');
const mail = require('bee-mail');
const queue = require('bee-queue');

const sendMessage = (msg) => {
  let message = msg.content.toString();
  let obj = JSON.parse(message);

  const params = {
      templateName: obj.template,
      message: obj.message
  }

  mail.messages.sendTemplate({
      template_name: params.templateName,
      template_content: [],
      message: params.message,
      async: true,
      ip_pool: 'Main Pool'
  }, function (result) {
      return console.log(null, result);
  }, function (e) {
      return console.log(e);
  });
}

const consume = (channel) => {
  async.forever((next) => {
    queue
      .getQueue(channel)
      .then((msg) => sendMessage(msg))
      .catch((err) => console.log(err));

    next();
  },
  function (err) {
    console.error(err);
  })
}

queue
  .connect()
  .then((channel) => consume(channel))
  .catch(function (err) {
    console.error('putz:', err.message);
  });
