# POC - Mail Queue

## Install

```console
git clone git@bitbucket.org:thiagopaes/poc-queue.git queue-poc
```

## Building

```console
docker-compose build
```

## Running

```console
docker-compose up
```

### Populate

```console
docker exec queuepoc_app_1 node src/populate.js
```

## Stopping

```console
docker-compose stop
```

## List queues

```console
docker exec queuepoc_queue_1 rabbitmqctl list_queues
```

## Management

To view management console, you can access `http://localhost:15672/#/queues` with default credentials: guest / guest.
